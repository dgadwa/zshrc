![Alt text](https://orig00.deviantart.net/b291/f/2015/026/b/2/cthulhu_by_banyai951-d8fgxwh.jpg)
# Dylan's .zshrc

## Version 0.2
* Changed sites alias from Dropbox to Google Drive.
* Added path for Yarn. 
* Added path for NVM.
* Added path for Composer.
* Added confirm script for rm command.
* Started confirm script (Inactive at present).
* Added notes for Dynamic Profiles (Local Open Site SSH).

## Version 0.1
* Did it up nice with some [Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Maybe one day, I'll hook up [NPM Pipeline](https://npme.npmjs.com/docs/tutorials/pipelines.html)

## Commands

* showh = Show hidden system files (.dotfiles)
* hideh = Show hidden system files (.dotfiles)
* sites = Show local development websites

## Dependencies

* [Oh My Zsh](http://ohmyz.sh/)

### Installing Oh My Zsh ###

* sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
		
## How do I put my own .zshrc (or .bash_profile) on git??

### Create folder called ~/.dotfiles in your home directory ###
* mkdir ~/.dotfiles

### Move your ~/.zshrc files (or ~/.bash_profile) to ~/.dotfiles ###
* mv ~/.zshrc ~/.dotfiles/zshrc
* mv ~/.bash_profile ~/.dotfiles/bash_profile

### Create symlinks to moved files ###
* ln -s ~/.dotfiles/zshrc ~/.zshrc
* ln -s ~/.dotfiles/bash_profile ~/.bash_profile

## How can I try this out? ##
* mkdir ~/.dotfiles (if you haven't done it already)
* cd .dotfiles
* git clone https://dgadwa@bitbucket.org/dgadwa/zshrc.git

![Alt text](https://image.ibb.co/bBAuRH/djg_zsh.jpg)
